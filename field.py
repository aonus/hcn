######################
# Create a HCN Field #
######################

%matplotlib inline
import yt
import numpy as np
from yt import derived_field
from scipy.interpolate import RectBivariateSpline as rbs
from yt.units.yt_array import YTQuantity as q
from matplotlib import pyplot as plt



#### Units and constants ####

pi = np.pi
km = 1.e5
pc = 3.0856e18
yr = 365.25*24.*3600.

c = 2.99792458e10
kB = 1.38065812e-16
mH = 2.34e-24	#effective mass per H nucleus
nH = 1.0e4  	#threshold volume density
rho = nH*mH		#threshold volume density in g/cm**3
nu = 88.631847e9 #line frequency of HCN(1-0)
lambda = c/nu	#wavelenght of HCN(1-0)

unitconversion1 = q(1.0, 'K*km*pc*pc/erg')
unitconversion2 = q(1.0, 'erg/s/g')
conversion = unitconverion1*lambda*lambda*lambda/8/pi/kB/pc/pc/km 	#convert erg/s to K km/s pc^2

bins = 100



#### Defining the field ####

file1 = "hcn_n.txt"										#list of number densities in H/cm**3 (log scale)
number = np.loadtxt(file1, delimiter=" ", dtype=None)
file2 = "hcn_dvdr.txt"									#list of velocity gradients in km/s/pc
dvdr = np.loadtxt(file2, delimiter=" ", dtype=None)
file3 = "hcn_lum2d.txt"									#list of corresponding HCN luminosities per H molecule (log scale)
HCN = np.loadtxt(file3, delimiter=" ", dtype=None)

x = number[:]+np.log10(mH) 		#log scale of mass density in g/cm^3
y = dvdr[:]-np.log10(pc/km) 	#log scale of velocity gradient in s^-1
z = HCN[:,:] 					#log scale of luminosity produced by HCN per molecule of H in erg/s/H
f = rbs(x,y,z)					#cubic spline interpolation

postprocess = "OUT_hdf5_plt_cnt_0131"										#post process of simulation script
ds = yt.load(postprocess)
dd = ds.all_data()
velocity_gradient = ds.add_gradient_fields(('gas', 'velocity_magnitude')) 	#replace with "velocity_x" for LOS gradient

def _HCN(field, data):
    dvdr3d = data['velocity_magnitude_gradient_magnitude']
    dens3d = data['density']
    mass = data['cell_mass']
    dvdr1d = np.ravel(dvdr3d)
    dens1d = np.ravel(dens3d)
    logdvdr = np.log10(dvdr1d)
    logdens = np.log10(dens)
    loglum = f(logdens,logdvdr, grid = False)
    lum1d = 10**(loglum)
    lum3d = lum1d.reshape(dens3d.shape)
    lHCN = mass*lum3d/mH
    return lHCN*unitconversion2
	
ds.add_field(('gas','HCN_Luminosity'), function=_HCN, units="erg/s")
ds.periodicity = (True, True, True)



#### Slice plots & Total luminosity ####

p1 = yt.SlicePlot(ds, "x", "HCN_Luminosity")	#Luminosity field in erg/s
#p1.set_zlim("HCN_Luminosity_per_cell", 3e19,3e25)
p1.show()

p2 = yt.SlicePlot(ds, "x", "density")
p2.set_zlim("density",3.0e-25,4.0e-19)
p2.show()

p3 = yt.SlicePlot(ds, "x", "velocity_magnitude_gradient_magnitude") #Local velocity gradient
p3.set_unit('velocity_magnitude_gradient_magnitude', 'km/s/pc')
#p3.set_zlim("velocity_magnitude_gradient_magnitude",5.0e-2,6.0e3)
p3.show()

HCNtotal = dd.quantities.total_quantity("HCN_Luminosity")
HCNtotal_infunnyunits = HCNtotal_inerg*conversion
print(HCNtotal_infunnyunits)



#### Density Profiles ####

rhomin,rhomax = dd.quantities.extrema("density")
mtotal = dd.quantities.total_quantity("cell_mass")
#HCNtotal = dd.quantities.total_quantity("HCN_Luminosity")	#already defined
logmin = np.log10(rhomin)
logmax = np.log10(rhomax)
rhoarray = np.logspace(logmin,logmax,bins)
logrange = np.log10(rhomax/rhomin)

massbin = []
HCNbin = []
for i in range(0,bins,1):
    density = dd["density"]
    if i==0:
        indices = np.where(rho <= rhoarray[i])
    if i==bins:
        indices = np.where(rho >= rhoarray[i])
    else:
        indices = np.where(np.logical_and(rho <= rhoarray[i], rho >= rhoarray[i-1])) 
    masssum = np.sum(dd["cell_mass"][indices])/(mtotal*logrange)
    lumsum = np.sum(dd['HCN_Luminosity'][indices])/(HCNtotal*logrange)
    massbin.append(masssum)
    HCNbin.append(lumsum)
	
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
ax1.semilogx(rhoarray, massbin, 'b-')
ax1.semilogx(rhoarray, HCNbin, 'g-')
ax1.set_xlabel("Density $(\mathrm{g/cm^3})$")
ax1.set_ylabel('Mass Distribution', color='b')
ax2.set_ylabel('$L_\mathrm{HCN}$ Distribution', color='g')
#plt.xlim(1.0e-23,1.0e-17)
#ax1.set_ylim(0.0,1.0e-2)
#ax2.set_ylim(0.0,1.0e-2)
ax2.set_yticklabels([])
plt.savefig('Density_Profile.eps')



#### alpha_HCN ####

threshold = dd["density"].in_units('g/cm**3') > rho
massthreshold = np.sum(dd["cell_mass"][threshold])
HCNthreshold = np.sum(dd["HCN_Luminosity"][threshold])
alpha = massthreshold/HCNthreshold/conversion
print(alpha.in_units('msun*s/(K*km*pc**2)'))