### What's in this Repository? ###

This repository contains the source code used to compute HCN
luminosity from simulations of a star-forming region, per the method
described in Onus, Krumholz, & Federrath (2018). It also contains the
code necessary to generate the lookup tables that this calculation
requires.

The contents are as follows:

* *field.py*: this script computes the HCN luminosity for each cell in
   specified simulation cube. The user specifies the file name of the
   output file to be processed (can be any field readable by
   [yt](http://yt-project.org)) and which HCN luminosity model to use
   (see below).
* *hcn_lum_tabulate.ipynb*: this python notebook contains the code
   required to generate the lookup tables of HCN luminosity versus gas
   density and velocity gradient for the six luminosity models
   described in Onus, Krumholz, & Federrath (2018). Note that the
   output of this calculation is included in the repository, so there
   is no need to re-run the notebook in order to use the *field.py*
   code. It is provided as a template for those who wish to create
   their own luminosity models.
* *hcn_n.txt* and *hcn_dvdr.txt*: these contain the list of densities
   and velocity gradients, respectively, used in the tables; they are
   written automatically by *hcn_lum_tabulate.ipynb*.
* *hcn_lum_std.txt*, *hcn_lum_HCNlow.txt*, *hcn_lum_HCNhigh.txt*,
  *hcn_lum_Thigh.txt*, *hcn_lum_Tvar.txt*: these contain tabulations
   of HCN luminosity per H nucleus as a function of density and
   temperature for the models "Standard", "Low HCN", "High HCN",
   "High Temp", and "Varied Temp" as described in the paper. Note that
   the "LOS" model uses the same input file as "Standard", so there is
   no separate file for it. This model is implemented by changing a
   line in the *field.py* file.

### Requirements ###

These scripts require the following:

* [numpy](http://www.numpy.org/)
* [scipy](http://www.scipy.org/)
* [yt](http://yt-project.org/)
* [despotic](https://bitbucket.org/krumholz/despotic/) -- only needed
  by *hcn_lum_tabulate.ipynb*, for generating HCN emission models; not
  needed otherwise